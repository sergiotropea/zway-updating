#!/usr/bin/env python
from __future__ import print_function
import json
import time
import os
import commands
import zway
import sys
import smtplib
import config
from email.mime.text import MIMEText as text

# status_timeout as first argument
if len(sys.argv) >1:
	print(sys.argv[1])
	status_timeout=int(sys.argv[1] or 120)
else:
	status_timeout=120

# max_queue as second argument
if len(sys.argv) >2:
	print(sys.argv[2])
	max_queue=int(sys.argv[2] or 1)
else:
	max_queue=1

while True:
	print("Zway STATUS Update ".ljust(40," ") + "@" + time.strftime("%c"))
	print("--------------------------------------------------------------------")
	print(" status_timeout: " + str(status_timeout) + " - max_queue: " + str(max_queue))
	print("--------------------------------------------------------------------")
	print("Loading devices...")
	print("--------------------------------------------------------------------")
	sys.stdout.flush()

	devices=zway.loadDevices();
	#devices=zway.loadDevicesFromFile("devices_list.json");

	updated=0
	updating=0
	n_of_dev=len(devices)-1
	startTime=int(time.time())

	zway.clearUpdateStats();
	for dev in devices:
		sys.stdout.flush()
		devName=dev.data.givenName.value or "(Unknown)"
		devIsFailed=dev.data.isFailed.value
		devType=dev.data.deviceTypeString.value or "(Unknown)"

		

		print("#" + str(dev.id).ljust(3," "), end=":")
		print(devName.ljust(50," "), end="  ")
		print(devType.ljust(22," "), end="  ")
		print("| ", end="")
		if zway.devIsFailed(dev):
			
			print("FAILED",end="")
			txt = "ID :: " + str(dev.id).ljust(3," ") + " - " + str(devName)  + " Dispositivo in Failed"


			print(str(dev.id).ljust(3," "))

			devId = dev.id
			sent=1
			try:
				devices = open("failed.txt","r").read()
				p = devices.split("\n")
				p.pop(len(p)-1)
				for i in range(0,len(p)):
					if devId == int(p[i]):
						sent=0
						print("Email inviata precedentemente")
						break;
			except:
				print("File non trovato");
			
			id = int(str(dev.id))
			if sent:
				out_file = open("failed.txt","a")
				out_file.write(str(dev.id) + "\n")
				out_file.close()

				m = text(txt)

				m['Subject'] = 'Demotic: Device ' + str(dev.id) + " in Controller " + config.API_url + " is failed"
				m['From'] = config.email_from
				m['To'] = config.email_to
				
				try:			
					s = smtplib.SMTP(config.email_smtp)
					s.starttls()
					s.login(config.email_username,config.email_password)
					s.sendmail(config.email_from, config.email_to , m.as_string())
					s.quit()
				except:
					print("Impossibie Inviare la Mail")
		else:
			if devType == "Binary Power Switch" or devType.startswith("Motor Control Device"):
				updating+= zway.devCommandGet(dev,37,status_timeout,max_queue)
			if devType.startswith("Motor Control Device"):
				print(" | ", end="")
				updating+= zway.devCommandGet(dev,38,status_timeout,max_queue)
		time.sleep(0.5)
		print(" X ")
	updated=n_of_dev-updating
	elapsed=int(time.time())-startTime

	print("--------------------------------------------------------------------")
	print("Updating: " + str(updating) + "/" + str(n_of_dev))
	print("Elapsed: " + str(elapsed) +"s")
	print("Update Average: " + str(zway.updateAverage()) +"s")
	print("--------------------------------------------------------------------")
	print("")
