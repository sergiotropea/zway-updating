from __future__ import print_function
import requests
from requests.auth import HTTPBasicAuth
import config
import json
import commands
import time
import sys
from bunch import bunchify

def printConfig():
    print("Zway Configuration:")
    print(" API_url="+config.API_url)
    print(" API_user="+config.API_user)
    print(" API_pass="+config.API_pass)
    return

def run(apiCommand):
    url=config.API_url + "/" + apiCommand
    try:
        resp=requests.get(url,auth=HTTPBasicAuth(config.API_user, config.API_pass))
        if resp.status_code != 200:
            return ""
        else:
            resp=resp.json()
            resp=bunchify(resp)
            return resp
    except Exception as e:
        print("ERROR: " + format(e))
        #raise

def loadDevice(devId):
    return run("Run/devices["+str(devId)+"]")

def loadDevices():
    devices=run("Run/devices")
    devices=sortDevices(devices)
    return devices

def loadDevicesFromFile(json_file):
    with open(json_file) as myfile:
    	devices=json.load(myfile)
    devices=bunchify(devices)
    devices=sortDevices(devices)
    return devices

def sortDevices(unordered_devices):
    ordered_devices=[]
    keys=sorted(unordered_devices.keys(), key=int)
    for key in keys:
    	ordered_devices.append(unordered_devices[key])
    return ordered_devices


def devCommandSupported(dev,commandClass):
    commandClass=str(commandClass)
    if not commandClass in dev.instances["0"].commandClasses:
        return False
    else:
        if dev.instances["0"].commandClasses[commandClass].data.supported.value == "false":
            return False
        else:
            return True

totalUpdateDelay=0
totalUpdateCount=0

def updateAverage():
	global totalUpdateDelay
	global totalUpdateCount

	avg=0
	if totalUpdateCount>0:		
		avg=totalUpdateDelay/totalUpdateCount
	return avg

def clearUpdateStats():
	global totalUpdateDelay
	global totalUpdateCount
	totalUpdateCount=0
	totalUpdateDelay=0

	
def devCommandGet(dev,commandClass,ttl,max_queue):
    global totalUpdateDelay
    global totalUpdateCount

    commandClass=str(commandClass)
    print("[" + commandClass+ "]", end=" ")
    if not devCommandSupported(dev,commandClass):
        print("NA", end="")
        return 0
    else:
        print(dev.instances["0"].commandClasses[commandClass].name, end="")
        val=dev.instances["0"].commandClasses[commandClass].data.level.value
        print("",end=" - ")
        nowTime=int(time.time())
        updateTime=run("Run/devices["+ str(dev.id) +"].instances[0].commandClasses[" + commandClass+ "].data.level.updateTime")
        print("(" + str(nowTime-updateTime) + "s) ", end="")
	totalUpdateDelay += nowTime-updateTime
	totalUpdateCount += 1

        if nowTime-updateTime > ttl:
            print(" Updating. ",end="")
            waitQueue(max_queue)
            run("Run/devices["+str(dev.id) + "].commandClasses["+ commandClass+ "].Get()")
            return 1
        else:
            print(" Keep",end="")
            return 0


def devIsFailed(dev):
    if dev is None:
        return True
    return dev.data.isFailed.value

def waitQueue(max_queue):
    while countQueue() >= max_queue:
        sys.stdout.flush()
        print(".",end="")
        time.sleep(0.5)
    return

def countQueue():
    queue=run("InspectQueue")
    count=0
    for job in queue:
    	timeout=job[0]
    	devId=job[2]
        status=job[4] or ""
	progress = job[3]
	#print (job[1][1])
	#print ("Queue: " + str(timeout) + " : " + str(devId) + " : " +str(status) +"\n")
	#print (progress)
	#print (str(progress).find("Wakeup"))
	if (str(progress).find("Wakeup") >= 0 
            or str(progress).find("Nonce") >=0 
            or str(progress).find("Clock") >=0 
            or str(progress).find("Schedule") >=0 
            or str(progress).find("Battery") >=0 
            or str(progress).find("V5") >=0 
            or job[1][1] == 1
	    ): 
	    #print ("Find: " + str(progress))
	    continue
        
	if timeout >= 0.20 and timeout <= 0.21:
            dev=loadDevice(devId)
            if not devIsFailed(dev):
                count=count+1
        else:
            if (status.find("Delivered") <0 
		and status.find("update done") <0 
		and status.find(": [") <0 
		and status.find("update failed") <0
		and status.find("is failed") <0
		and status.find("is operating") <0
		and status.find("Removing job") <0
		):
                count=count+1
    
    #print (count)
    return count
